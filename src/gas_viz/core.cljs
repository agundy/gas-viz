(ns gas-viz.core
    (:require
      [reagent.core :as r]
      [reagent.dom :as d]
      [goog.string :as gstring]
      [goog.string.format]))

(def ^:const federal-tax 0.184)

;; -------------------------
;; Components

(defn monthly-gallons [distance mpg]
  (/ (* distance 4) mpg))

(defn calc-fed-tax-cost [gallons]
  (* gallons federal-tax)
  )

(defn driver-category [gals]
  (cond
    (< gals 7) ["green" "Earth Friendly"]
    (< gals 14) ["green" "Economical"]
    (< gals 25) ["orange" "Room for Improvement"]
    (< gals 51) ["orange" "Over Driving"]
    :else ["red" "Earth Destroyer"]
   ))

(defn calc-fuel-costs [{:keys [mpg distance price cost] :as data}]
  (let [gals (monthly-gallons distance mpg)]
    (if (nil? cost)
      (assoc data :cost (* price gals) :gallons gals :fed-tax (calc-fed-tax-cost gals))
      (assoc data :distance (* cost mpg):gallons gals :fed-tax (calc-fed-tax-cost gals)))))

(def fuel-data (r/atom (calc-fuel-costs {:mpg 24 :distance 300 :price 3.20})))

(defn float-slider [param value min max invalidates]
  [:input {:type "range" :value value :min min :max max :step 0.05
           :style {:width "100%"}
           :on-change (fn [e]
                        (let [new-value (js/parseFloat (.. e -target -value))]
                          (swap! fuel-data
                                 (fn [data]
                                   (-> data
                                     (assoc param new-value)
                                     (dissoc invalidates)
                                     calc-fuel-costs)))))}])

(defn slider [param value min max invalidates]
  [:input {:type "range" :value value :min min :max max
           :style {:width "100%"}
           :on-change (fn [e]
                        (let [new-value (js/parseInt (.. e -target -value))]
                          (swap! fuel-data
                                 (fn [data]
                                   (-> data
                                     (assoc param new-value)
                                     (dissoc invalidates)
                                     calc-fuel-costs)))))}])

(defn input-component []
  (let [{:keys [distance mpg price cost fed-tax gallons]} @fuel-data
        [color diagnose] (driver-category gallons)]
    [:div
     [:h3 "Variables"]
     [:div
      "Price $" (gstring/format "%.2f" price) " per gallon"
      [float-slider :price price 2 6 :cost]]
     [:div
      "Distance " (int distance) " miles a week"
      [slider :distance distance 0 600 :cost]]
     [:div
      "Fuel Economy " (int mpg) " MPG"
      [slider :mpg mpg 2 55 :cost]]
     [:h3 "Costs"]
     [:div
      "Driver type: " [:span {:style {:color color}} diagnose]]
     [:div
      "Fuel Costs $" (gstring/format "%.2f" cost) " per month "]
     [:div {:style {:margin-top "2em"}}
      "Federal gas tax suspension would only save you "
      [:span {:style {:font-weight "bold"}} (gstring/format "$%.2f" fed-tax)]
      " per month this year."
      ]
     [:div {:style {:margin-top "2em"}}
      "Driving 15% less would save you "
      [:span {:style {:font-weight "bold"}} (gstring/format "$%.2f" (* 0.15 cost))]
      " per month in fuel this year."
      ]]))

;; -------------------------
;; Views

(defn home-page []
  [:div [:h2 "Gas Calculator"]
  [input-component]])

;; -------------------------
;; Initialize app

(defn mount-root []
  (d/render [home-page] (.getElementById js/document "app")))

(defn ^:export init! []
  (mount-root))
